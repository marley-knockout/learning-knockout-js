# Learning Knockout JS

**Original src included**

<br/>

### Section 2: Displaying Data Using KnockoutJS

<br/>

**01 Creating models views and view models**

We will create a website with the necessary Models, Views, and ViewModels to establish a KnockoutJS application.

![Application](/img/Section_2_01_creating_models_views_and_view_models.png?raw=true)


<br/>

**02 Displaying model data in a view**

In order to display our model's data, we need to add data-binding syntax to the markup in our View.

![Application](/img/Section_2_02_displaying_model_data_in_a_view.png?raw=true)

<br/>

**03 Data Binding Html Attributes**

There are several ways to data-bind attributes on HTML elements with KnockoutJS. We will take a look at three ways of manipulating attributes with KnockoutJS.

![Application](/img/Section_2_03_data_binding_html_attributes.png?raw=true)


<br/>

### Section 3: Two-way Data Binding in KnockoutJS

<br/>

**01 Updating the Model from the View and Vice Versa**

We will take a closer look at how to update the View Model from an HTML form element as well as populate an HTML form element with data from the View Model.

http://192.168.99.100:3000/data-binding.html

![Application](/img/section_03_pic_01.png?raw=true)

<br/>

**02 Data Binding Form Fields**

Investigate how to data-bind various HTML form elements with KnockoutJS

http://192.168.99.100:3000/data-binding.html

![Application](/img/section_03_pic_02.png?raw=true)


<br/>

**03 Controlling Form Fields with Data Binding**

Demonstrate additional bindings that can help control the appearance and functionality of HTML form elements.

![Application](/img/section_03_pic_03.png?raw=true)

<br/>

### Section 4: Understanding Context in KnockoutJS

<br/>

**01 Examining the Different Types of Context**

Context is fundamental to the MVVM pattern and data-binding. Yet, it's often misunderstood. Demonstrate and explain the commonly used context objects in KnockoutJS.


![Application](/img/section_04_pic_01.png?raw=true)

![Application](/img/section_04_pic_02.png?raw=true)


<br/>

**02 Binding with Templates**

Templates allow Knockout developers to consolidate and share UI elements among many data-binding scenarios. Templates create a new binding context; it's necessary to understand how to deal with the new context.

http://192.168.99.100:3000/external-template-books.html

![Application](/img/section_04_pic_03.png?raw=true)

<br/>

http://192.168.99.100:3000/external-template-magzines.html

![Application](/img/section_04_pic_04.png?raw=true)


<br/>

**03 Using Multiple and Nesting ViewModels**

Sometimes, it's necessary to use multiple ViewModels in a single View. We examine a few methods to compose a View and View Model using multiple ViewModels.


http://192.168.99.100:3000/multiple-apply-bindings.html

![Application](/img/section_04_pic_05.png?raw=true)

<br/>

http://192.168.99.100:3000/composition.html

![Application](/img/section_04_pic_06.png?raw=true)


<br/>

### Section 5: Using Computed Observables and Subscriptions

<br/>

**01 Creating Computed and Pure Computed Observables**

There are times when you want to bind to a different representation of your data than what is provided in the raw data. Using computed and pureComputed observables, you can accomplish this task.


**!!! BEFORE TESTS, REMOVE FROM BROWSER LOCAL STORAGE "CATALOG" !!!**

**SORT BY TYPE**

http://192.168.99.100:3000/media.html

![Application](/img/section_05_pic_01.png?raw=true)

<br/>

**SORT BY NAME**

http://192.168.99.100:3000/borrowers.html

![Application](/img/section_05_pic_02.png?raw=true)

<br/>

http://192.168.99.100:3000/default.html

![Application](/img/section_05_pic_03.png?raw=true)


<br/>

http://192.168.99.100:3000/computed.html

![Application](/img/section_05_pic_04.png?raw=true)


<br/>

**02 Subscribing to Observables**

Subscribing to observables allows you to intercept changes made to them and perform additional tasks.

http://192.168.99.100:3000/subscribe.html

![Application](/img/section_05_pic_05.png?raw=true)


<br/>

### Section 6: Custom Bindings, Custom Functions, and Extenders

<br/>

**01 Creating Custom Functions**

You can attach custom functions to Knockout's core value types to extend their functionality.

http://192.168.99.100:3000/custom-function.html

![Application](/img/section_06_pic_01.png?raw=true)

<br/>

**02 Creating Extenders**

Extenders allow you to augment the functionality of observables by adding new properties.

<br/>

http://192.168.99.100:3000/extender.html

![Application](/img/section_06_pic_02.png?raw=true)

<br/>

**additional, as example (look code in original src)**

![Application](/img/section_06_pic_03.png?raw=true)


<br/>

**03 Interacting with jQuery in Custom Bindings**

Custom bindings are a way of encapsulating behavior and exposing it in the same way in which you use 'text' or 'foreach' bindings.

<br/>

http://192.168.99.100:3000/media.html

![Application](/img/section_06_pic_04.png?raw=true)


<br/>

**additional, as example (look code in original src)**

<br/>

![Application](/img/section_06_pic_05.png?raw=true)


<br/>

![Application](/img/section_06_pic_06.png?raw=true)




<br/>

### Section 7: Previewing Advanced KnockoutJS

<br/>

**01 Using Components and Custom Elements**

Components and custom elements allow you to consolidate often-used code into a single, reusable unit.

http://192.168.99.100:3000/component.html

<br/>

![Application](/img/section_07_pic_01.png?raw=true)


<br/>

**additional, as example (look code in original src)**

<br/>

![Application](/img/section_07_pic_02.png?raw=true)



<br/>

**02 Using AMD Module Loaders**

AMD module loaders make it easy to modularize your code and load dependencies.

**require.js**

<br/>

http://192.168.99.100:3000/require.html


![Application](/img/section_07_pic_03.png?raw=true)


<br/>

**03 Examining Durandal to Build Single Page Applications (SPA)**

Durandal is a framework that uses KnockoutJS and RequireJS to build single-page applications.

durandaljs.com

<br/>

http://192.168.99.100:3000/durandal.html


![Application](/img/section_07_pic_04.png?raw=true)
